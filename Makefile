ifdef OS
	SHELLTYPE = batch
	# how can we check if we are in powershell?
	PYTHON = python
	RM = del /Q
	CP = copy /Y
	FixPath = $(subst /,\,$1)
else
	# Linux and Mac can be detected as follows; but are treated the same:
	# ifeq ($(shell uname), Linux)
	# ifeq ($(shell uname), Darwin)
	SHELLTYPE = bash
	PYTHON = python3
	RM = rm -f
	CP = cp
	FixPath = $1
endif

TMPFILES = *.aux *.bbl *.fls *.fdb_latexmk *.toc *.bcf *.run.xml *.blg *.log *.out *.synctex.gz
.PHONY: all xelatex_fourier xelatex_roboto pdflatex_fourier pdflatex_roboto dissertation_lyx clean dist_clean

# define default targets for `make`
all: pdflatex_roboto pdflatex_fourier xelatex_roboto xelatex_fourier dissertation_lyx

clean:
	del /s /q $(TMPFILES) || rm $(TMPFILES) */*.aux ||:

dist_clean:
	make clean
	del *.pdf || rm *.pdf ||:
# targets are never made, so make tasks below are always executed.

pdflatex_roboto:
	$(PYTHON) _python/render_tex_source.py dissertation
	$(CP) dissertation.pdf dissertation_$@.pdf
	$(PYTHON) _python/render_tex_source.py propositions
	$(CP) propositions.pdf propositions_$@.pdf

pdflatex_fourier:
	$(PYTHON) _python/render_tex_source.py dissertation --class-options fourier
	$(CP) dissertation.pdf dissertation_$@.pdf
	$(PYTHON) _python/render_tex_source.py propositions --class-options fourier
	$(CP) propositions.pdf propositions_$@.pdf

xelatex_roboto:
	$(PYTHON) _python/render_tex_source.py dissertation --compiler xelatex
	$(CP) dissertation.pdf dissertation_$@.pdf
	$(PYTHON) _python/render_tex_source.py propositions --compiler xelatex
	$(CP) propositions.pdf propositions_$@.pdf

xelatex_fourier:
	$(PYTHON) _python/render_tex_source.py dissertation --compiler xelatex --class-options fourier
	$(CP) dissertation.pdf dissertation_$@.pdf
	$(PYTHON) _python/render_tex_source.py propositions --compiler xelatex --class-options fourier
	$(CP) propositions.pdf propositions_$@.pdf

dissertation_lyx:
#	in one step, but will not give clear feedback in case of error:
#	lyx --export pdf4 dissertation.lyx
	lyx --force-overwrite --export xetex $@.lyx
	$(PYTHON) _python/render_tex_source.py $@ --compiler xelatex

font_test:
	xelatex "\input{_style/font-test.tex}"
	$(CP) "font-test.pdf" "_style/font-roboto-xelatex.pdf"
	xelatex "\PassOptionsToPackage{fourier}{_style/fonts-tudelft}\input{_style/font-test.tex}"
	$(CP) "font-test.pdf" "_style/font-fourier-xelatex.pdf"
	pdflatex "\input{_style/font-test.tex}"
	$(CP) "font-test.pdf" "_style/font-roboto-pdflatex.pdf"
	pdflatex "\PassOptionsToPackage{fourier}{_style/fonts-tudelft}\input{_style/font-test.tex}"
	$(CP) "font-test.pdf" "_style/font-fourier-pdflatex.pdf"
	cd _style && pdflatex font-test-table
	mv _style/font-test-table.pdf .
