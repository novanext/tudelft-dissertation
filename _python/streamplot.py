import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

plt.style.use(str(Path(__file__).parent / 'utopia.mplstyle'))


L = 3
Y, X = np.mgrid[-L:L:100j, -L:L:100j]
U = -1 - X**2 + Y
V = 1 + X - Y**2
speed = np.sqrt(U*U + V*V)

fig, ax = plt.subplots(1, 1)
im = ax.imshow(speed, extent=[-L, L, -L, L], alpha=0.5)
plt.colorbar(im, label='speed (m/s)')

ax.streamplot(X, Y, U, V, linewidth=0.2*speed)

ax.set(
    title='Streamlines',
    xlabel='$x$ (m)',
    ylabel='$y$ (m)',
)

# plt.show()
fig.savefig('streamplot.png')
fig.savefig('streamplot.pdf')
