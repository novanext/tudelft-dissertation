import asyncio
import sys


async def run_async(cmd, return_exit_code=False):
    proc = await asyncio.create_subprocess_exec(
        *cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    queue = asyncio.Queue()
    tasks = (
        asyncio.create_task(pipe_to_queue(proc.stdout, queue, 1)),
        asyncio.create_task(pipe_to_queue(proc.stderr, queue, 2)),
    )

    errors = []
    while not all([t.done() for t in tasks]):
        await asyncio.sleep(.1)
        while not(queue.empty()):
            stream_nr, data = await queue.get()
            if stream_nr == 2:
                errors.append(data.decode('utf8'))
            print(data.decode('utf8', errors='replace'), end='')

    await proc.wait()
    if proc.returncode:
        print(f"Return code: {proc.returncode}", file=sys.stderr)
        if return_exit_code:
            exit(proc.returncode)
    if errors:
        print('Errors:', ''.join(errors), sep='\n', file=sys.stderr)
        exit(-1)


async def pipe_to_queue(pipe, queue, prefix=None):
    while True:
        data = await pipe.readline()
        if data == b'':
            break
        await queue.put((prefix, data))

if sys.platform == "win32":
    asyncio.set_event_loop_policy(
        asyncio.WindowsProactorEventLoopPolicy())


def run(cmd, **kwargs):
    asyncio.run(run_async(cmd, **kwargs))


if __name__ == '__main__':
    cmd = 'python', '_python/generate_example_output.py'
    asyncio.run(run_async(cmd, return_exit_code=False))
