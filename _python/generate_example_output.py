
import sys
import time

time.sleep(1)
print('out 1', file=sys.stdout)
sys.stdout.flush()
sys.stderr.flush()

time.sleep(1)
print('err 1', file=sys.stderr)
sys.stdout.flush()
sys.stderr.flush()

time.sleep(1)
print('err 2', file=sys.stderr)
sys.stdout.flush()
sys.stderr.flush()

time.sleep(1)
print('out 2', file=sys.stdout)
sys.stdout.flush()
sys.stderr.flush()

time.sleep(.2)
exit(2)
