import click
import hashlib
import os
import sys
import subprocess
import execution_helper

# BUF_SIZE is totally arbitrary, change for your app!
BUF_SIZE = 64*2**10  # let's read stuff in 64kb chunks!


def file_hash(in_file, hash_function):
    with open(in_file, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            hash_function.update(data)
    return hash_function.hexdigest()


@click.command()
@click.argument('source', nargs=1)
@click.option("--compiler", default='pdflatex', help="tex compiler")
@click.option("--bib_compiler", default='biber', help="bib compiler")
@click.option(
    "--class-options", default='',
    help="comma-separated options passed to dissertation class"
)
def click_command(source, compiler, bib_compiler, class_options):
    """
    Render tex source file (without extension) using given compiler and options
    """
    assert('.' not in source)
    assert(' ' not in source)
    tex_command = (
        compiler,
        '-file-line-error',
        "-synctex=1",
        "-interaction=nonstopmode",
        f"\\PassOptionsToClass{{{class_options}}}{{_style/dissertation}}"
        + f"\\input{{{source}}}",
    )
    bib_command = (
        bib_compiler,
        source,
    )

    def get_hash(extension):
        in_file = '.'.join((source, extension))
        if os.path.isfile(in_file):
            h = file_hash(in_file, hashlib.md5())
            print(in_file, h)
            return h

    aux_hash_1 = get_hash('aux')
    bbl_hash_1 = get_hash('bbl')
    execution_helper.run(tex_command, return_exit_code=False)
    aux_hash_2 = get_hash('aux')

    if aux_hash_1 == aux_hash_2 and bbl_hash_1 is not None:
        print('skipping bib and tex rerun')
        exit()

    execution_helper.run(bib_command)
    execution_helper.run(tex_command)
    aux_hash_3 = get_hash('aux')

    if aux_hash_3 == aux_hash_2:
        print('skipping tex rerun')
        exit()

    execution_helper.run(tex_command)


if __name__ == '__main__':
    click_command()
